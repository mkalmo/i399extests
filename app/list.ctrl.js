(function () {
    'use strict';

    angular.module('app').controller('ListCtrl', Ctrl);

    function Ctrl($http) {
        var vm = this;
        vm.tasks = [];
        vm.newTitle = '';

        vm.addTask = addTask;

        init();

        function init() {
            $http.get('api/tasks').then(function (result) {
                vm.tasks = result.data;
            });
        }

        function addTask() {
            var newTask = {
                title: vm.newTitle,
                added: new Date()
            };

            $http.post('api/tasks', newTask); // TODO: refresh list after insert

            vm.newTitle = '';
        }

    }
})();

